﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Asylum.Controller;

namespace Asylum.UnitTest
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void DoctorIndex()
        {            
            DoctorController controller = new DoctorController();
                        
            ViewResult result = controller.Index() as ViewResult;
                        
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void PatientIndex()
        {            
            PatientController controller = new PatientController();
            
            ViewResult result = controller.Index() as ViewResult;
            
            Assert.IsNotNull(result);
        }
    }
}
