﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Asylum.Model;

namespace Asylum.Controller
{
    public class MedicationController : System.Web.Mvc.Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Medication
        public ActionResult Index()
        {
            return View(db.Medications.ToList());
        }

        // GET: Medication/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medication medicationModel = db.Medications.Find(id);
            if (medicationModel == null)
            {
                return HttpNotFound();
            }
            return View(medicationModel);
        }

        // GET: Medication/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Medication/Create        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Manufacturer,Name,Dosage")] Medication medicationModel)
        {
            if (ModelState.IsValid)
            {
                db.Medications.Add(medicationModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(medicationModel);
        }

        // GET: Medication/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medication medicationModel = db.Medications.Find(id);
            if (medicationModel == null)
            {
                return HttpNotFound();
            }
            return View(medicationModel);
        }

        // POST: Medication/Edit/5        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Manufacturer,Name,Dosage")] Medication medicationModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(medicationModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(medicationModel);
        }

        // GET: Medication/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medication medicationModel = db.Medications.Find(id);
            if (medicationModel == null)
            {
                return HttpNotFound();
            }
            return View(medicationModel);
        }

        // POST: Medication/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Medication medicationModel = db.Medications.Find(id);
            db.Medications.Remove(medicationModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
