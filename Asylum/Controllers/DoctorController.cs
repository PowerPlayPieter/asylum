﻿using System.Net;
using System.Linq;
using Asylum.Model;
using Asylum.Model.ViewModels;
using System.Web.Mvc;
using System.Data.Entity;

namespace Asylum.Controller
{
    public class DoctorController : System.Web.Mvc.Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Doctor
        public ActionResult Index()
        {
            var doctors = _db.Doctors.ToList();
            var vms = doctors.Select(doctor => new DoctorVm()
                {
                    Id = doctor.Id,
                    Title = doctor.Title,
                    FirstName = doctor.FirstName,
                    LastName = doctor.LastName,
                    Email = doctor.Email,
                    DateOfBirth = doctor.DateOfBirth
                })
                .ToList();

            return View(vms);
        }

        // GET: Doctor/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }            
            var doctor = _db.Doctors.Find(id);
            if (doctor == null)
            {
                return HttpNotFound();
            }

            var vm = new DoctorVm
            {
                Id = doctor.Id,
                Title = doctor.Title,
                FirstName = doctor.FirstName,
                LastName = doctor.LastName,
                Email = doctor.Email,
                DateOfBirth = doctor.DateOfBirth,
                Patients = doctor.Patients
            };
            
            return View(vm);
        }

        // GET: Doctor/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Doctor/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Title,FirstName,LastName,Email,DateOfBirth")] DoctorVm vm)
        {
            var dm = new Doctor
            {
                Id = vm.Id,
                Title = vm.Title,
                FirstName = vm.FirstName,
                LastName = vm.LastName,
                Email = vm.Email,
                DateOfBirth = vm.DateOfBirth
            };

            if (!ModelState.IsValid) return View(vm);
            _db.Doctors.Add(dm);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Doctor/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var doctor = _db.Doctors.Find(id);
            if (doctor == null)
            {
                return HttpNotFound();
            }

            var vm = new DoctorVm
            {
                Id = doctor.Id,
                Title = doctor.Title,
                FirstName = doctor.FirstName,
                LastName = doctor.LastName,
                Email = doctor.Email,
                DateOfBirth = doctor.DateOfBirth
            };

            return View(vm);            
        }

        // POST: Doctor/Edit/5        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Title,FirstName,LastName,Email,DateOfBirth")] DoctorVm vm)
        {
            var dm = new Doctor
            {
                Id = vm.Id,
                Title = vm.Title,
                FirstName = vm.FirstName,
                LastName = vm.LastName,
                Email = vm.Email,
                DateOfBirth = vm.DateOfBirth
            };

            if (!ModelState.IsValid) return View(vm);
            _db.Entry(dm).State = EntityState.Modified;
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Doctor/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var doctor = _db.Doctors.Find(id);
            if (doctor == null)
            {
                return HttpNotFound();
            }

            var vm = new DoctorVm
            {
                Id = doctor.Id,
                Title = doctor.Title,
                FirstName = doctor.FirstName,
                LastName = doctor.LastName,
                Email = doctor.Email,
                DateOfBirth = doctor.DateOfBirth
            };

            return View(vm);            
        }

        // POST: Doctor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Doctor doctorModel = _db.Doctors.Find(id);
            if (doctorModel != null) _db.Doctors.Remove(doctorModel);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
