﻿using System.IO;
using System.Net;
using System.Web;
using System.Linq;
using Asylum.Model;
using System.Web.Mvc;
using Asylum.Model.ViewModels;
using System.Data.Entity;
using System.Collections.Generic;

namespace Asylum.Controller
{
    public class PatientController : System.Web.Mvc.Controller
    {
        private  ApplicationDbContext db = new ApplicationDbContext();
        
        // GET: Patient
        public ActionResult Index()
        {
            var patients = db.Patients.Include(i => i.Doctor).ToList();
            var vms = new List<PatientVm>();

            foreach (var patient in patients)
            {
                vms.Add(new PatientVm()
                {
                    Id = patient.Id,
                    Title = patient.Title,
                    FirstName = patient.FirstName,
                    LastName = patient.LastName,
                    Gender = patient.Gender,
                    DateOfBirth = patient.DateOfBirth,
                    DateOfAdmission = patient.DateOfAdmission,
                    Doctor = patient.Doctor
                });
            }
            return View(vms);            
        }

        // GET: Patient/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var patient = db.Patients
                .Include(i => i.Doctor)
                .Single(i => i.Id == id);


            if (patient == null)
            {
                return HttpNotFound();
            }

            var vm = new PatientVm
            {
                Id = patient.Id,
                Title = patient.Title,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                Gender = patient.Gender,
                DateOfBirth = patient.DateOfBirth,
                DateOfAdmission = patient.DateOfAdmission,
                Doctor = patient.Doctor,
                Photo = patient.Photo,
                Image = patient.Image,
                Medications = patient.Medications,
                Doctors = patient.Doctors

            };
            return View(vm);            
        }

        // GET: Patient/Create
        public ActionResult Create()
        {            
            ViewBag.DoctorId = new SelectList(db.Doctors, "ID", "FullName");
            return View();
        }

        //POST: Patient/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Title,FirstName,LastName,DateOfBirth,Gender,DateOfAdmission,Image,Photo,DoctorId")] Patient patientModel, HttpPostedFileBase file)
        {
            var newPatient = new PatientVm();

            if (ModelState.IsValid)
            {

                // Update fields
                if (file != null && file.ContentLength > 0)
                {
                    byte[] imgBinaryData = new byte[file.ContentLength];
                    patientModel.Image = imgBinaryData;
                    file.InputStream.Read(patientModel.Image, 0, file.ContentLength);
                    patientModel.Photo = Path.GetFileName(file.FileName);
                }

                newPatient.Title = patientModel.Title;
                newPatient.Gender = patientModel.Gender;
                newPatient.FirstName = patientModel.FirstName;
                newPatient.LastName = patientModel.LastName;
                newPatient.DateOfAdmission = patientModel.DateOfAdmission;
                newPatient.DateOfBirth = patientModel.DateOfBirth;
                newPatient.DoctorId = patientModel.DoctorId;
                newPatient.Doctor = patientModel.Doctor;

                db.Patients.Add(patientModel);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DoctorId = new SelectList(db.Doctors, "ID", "FullName", patientModel.DoctorId);
            return View(newPatient);
            
        }

        // GET: Patient/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var patient = db.Patients
                .Include(i => i.Medications)
                .Single(i => i.Id == id);
            PopulatePrescribedMedicationData(patient);

            if (patient == null)
            {
                return HttpNotFound();
            }

            PatientVm vm = new PatientVm
            {
                Id = patient.Id,
                Title = patient.Title,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                Gender = patient.Gender,
                DateOfBirth = patient.DateOfBirth,
                DateOfAdmission = patient.DateOfAdmission,
                Doctor = patient.Doctor,
                Photo = patient.Photo,
                Image = patient.Image,
                Medications = patient.Medications,
                Doctors = patient.Doctors,
                DoctorId = patient.DoctorId
            };

            //Used by the drop down list
            ViewBag.DoctorId = new SelectList(db.Doctors, "ID", "FullName", vm.DoctorId);

            return View(vm);
            
        }

        private void PopulatePrescribedMedicationData(Patient patient)
        {
            var allMedications = db.Medications;
            var patientMedications = new HashSet<int>(patient.Medications.Select(c => c.ID));
            var viewModel = new List<PrescribedMedicationData>();
            foreach (var medication in allMedications)
            {
                viewModel.Add(new PrescribedMedicationData
                {
                    MedicationID = medication.ID,
                    Manufacturer = medication.Manufacturer,
                    Name = medication.Name,
                    Dosage = medication.Dosage,
                    Prescribed = patientMedications.Contains(medication.ID)
                });
            }
            ViewBag.Medications = viewModel;
        }

        // POST: Patient/Edit/5
        [HttpPost]
        public ActionResult Edit([Bind(Exclude = "Image")]PatientVm vm, HttpPostedFileBase file, string[] selectedMedications)
        {
            //Uncomment the following if statement and code block if you wish to save a copy of the file to folder
            //if (file != null && file.ContentLength > 0)
            //{
            // extract only the fieldname
            //var fileName = Path.GetFileName(file.FileName);
            // store the file inside ~/App_Data/Profiles/Mugshots folder
            //var path = Path.Combine(Server.MapPath("~/App_Data/Profiles/Mugshots"), fileName);
            //file.SaveAs(path);
            //}

            if (ModelState.IsValid)
            {
                // Get the patient to update             
                var patientToUpdate = db.Patients
                    .Include(i => i.Medications)
                    .Single(i => i.Id == vm.Id);

                // Update fields
                if (file != null && file.ContentLength > 0)
                {
                    var imgBinaryData = new byte[file.ContentLength];
                    patientToUpdate.Image = imgBinaryData;
                    file.InputStream.Read(patientToUpdate.Image, 0, file.ContentLength);
                    patientToUpdate.Photo = Path.GetFileName(file.FileName);
                }

                patientToUpdate.Title = vm.Title;
                patientToUpdate.Gender = vm.Gender;
                patientToUpdate.FirstName = vm.FirstName;
                patientToUpdate.LastName = vm.LastName;
                patientToUpdate.DateOfAdmission = vm.DateOfAdmission;
                patientToUpdate.DateOfBirth = vm.DateOfBirth;
                patientToUpdate.DoctorId = vm.DoctorId;
                patientToUpdate.Doctor = vm.Doctor;

                db.Entry(patientToUpdate).State = EntityState.Modified;
                
                UpdatePatientMedications(selectedMedications, patientToUpdate);
                
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.DoctorId = new SelectList(db.Doctors, "ID", "Title", vm.DoctorId);

            return View(vm);            
        }

        public void UpdatePatientMedications(string[] selectedMedications, Patient patientToUpdate)
        {
            if (selectedMedications == null)
            {
                patientToUpdate.Medications = new List<Medication>();
                return;
            }

            var selectedMedicationsHs = new HashSet<string>(selectedMedications);
            var patientMedications = new HashSet<int>(patientToUpdate.Medications.Select(c => c.ID));
            foreach (var medication in db.Medications)
            {
                if (selectedMedicationsHs.Contains(medication.ID.ToString()))
                {
                    if (!patientMedications.Contains(medication.ID))
                    {
                        patientToUpdate.Medications.Add(medication);
                    }
                }
                else
                {
                    if (patientMedications.Contains(medication.ID))
                    {
                        patientToUpdate.Medications.Remove(medication);
                    }
                }
            }
        }

        // GET: Patient/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var patient = db.Patients.Find(id);
            if (patient == null)
            {
                return HttpNotFound();
            }

            PatientVm VM = new PatientVm
            {
                Id = patient.Id,
                Title = patient.Title,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                Gender = patient.Gender,
                DateOfBirth = patient.DateOfBirth,
                DateOfAdmission = patient.DateOfAdmission,
                Doctor = patient.Doctor,
                Photo = patient.Photo,
                Image = patient.Image,
                Medications = patient.Medications,
                Doctors = patient.Doctors,
                DoctorId = patient.DoctorId
            };

            return View(VM);
           
        }

        // POST: Patient/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Patient patientModel = db.Patients.Find(id);
            if (patientModel != null) db.Patients.Remove(patientModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
