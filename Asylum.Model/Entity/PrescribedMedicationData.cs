﻿namespace Asylum.Model
{
    public class PrescribedMedicationData
    {
        public int MedicationID { get; set; }
        public string Manufacturer { get; set; }
        public string Name { get; set; }
        public string Dosage { get; set; }
        public bool Prescribed { get; set; }
    }
}