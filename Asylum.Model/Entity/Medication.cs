﻿using System.Collections.Generic;

namespace Asylum.Model
{
    public class Medication
    {
        public int ID { get; set; }
        public string Manufacturer { get; set; }
        public string Name { get; set; }
        public string Dosage { get; set; }
        public bool Prescribed { get; set; }

        public virtual ICollection<Patient> Patients { get; set; }        
    }
}