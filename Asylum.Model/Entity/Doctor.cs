﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asylum.Model
{
    public class Doctor : Person
    {
        public string Email { get; set; }

        public virtual ICollection<Patient> Patients { get; set; }
    }
}
