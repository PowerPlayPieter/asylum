﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asylum.Model
{
    public class Patient : Person
    {
        public string Photo { get; set; }

        public byte[] Image { get; set; }

        [ForeignKey("Doctor")]
        public int? DoctorId { get; set; }    
        
        public virtual Doctor Doctor { get; set; }
        
        public virtual IEnumerable<Doctor> Doctors { get; set; }

        public virtual ICollection<Medication> Medications { get; set; }
    }
}