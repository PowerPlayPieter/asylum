﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Asylum.Model.ViewModels
{
    public class DoctorVm
    {        
        public int Id { get; set; }

        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required, MaxLength(50)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required, MaxLength(50)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Doctor")]
        public string FullName => $"{Title} {FirstName} {LastName}";

        [Display(Name = "Gender")]
        public string Gender { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date of Birth")]
        public DateTime? DateOfBirth { get; set; }

        public virtual ICollection<Patient> Patients { get; set; }
    }
}