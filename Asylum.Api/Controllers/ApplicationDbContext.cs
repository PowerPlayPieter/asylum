﻿using System.Data.Entity;

namespace Asylum.Controller
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Asylum.Model.Person> People { get; set; }
        public DbSet<Asylum.Model.Employee> Employees { get; set; }
        public DbSet<Asylum.Model.Doctor> Doctors { get; set; }
        public DbSet<Asylum.Model.Patient> Patients { get; set; }
        public DbSet<Asylum.Model.Medication> Medications { get; set; }
    }
}