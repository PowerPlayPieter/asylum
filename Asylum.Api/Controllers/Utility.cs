﻿using System.Linq;
using Asylum.Controller;

namespace Asylum.Api.Controller
{
    public class Utility
    {
        private static readonly ApplicationDbContext Db = new ApplicationDbContext();
        
        public static int CountPatients()
        {
            return Db.Patients.Count();
        }

        public static int CountDoctors()
        {
            return Db.Doctors.Count();            
        }

        public static int AccommodationAvailable()
        {
            return 500 - CountPatients();
        }
    }
}