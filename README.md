# README #

### What is this repository for? ###

* Practice MVC with Entity Framework

### How do I get set up? ###
* Clone and build

### Still to do ###
* Register/Login
* Unit tests
* API
* Seperate DAL
* Mongo DB
* Dependency Injection

### Who do I talk to? ###

* Pieter du Toit
